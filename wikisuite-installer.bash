#!/usr/bin/env bash
# shellcheck disable=SC2034 disable=SC2154
# WikiSuite and Virtualmin GPL Stack Installer
# Script: wikisuite-installer.bash
# Version: 2.0.2
# Copyright: 2011-2025 WikiSuite
#
# Usage:
#   wikisuite-installer.bash [options]
#   Example: bash wikisuite-installer.bash --bundle LEMP --features ssl,dns
#
# Notes:
#   - This script requires root or sudo privileges.
#   - Ensure all dependencies are met before running the installer.
#

# Version of the installer
TVER=2.0.2

# Ensure the script is being run in Bash. The script uses Bash-specific
# features, so exit if another shell is being used.
if [ -z "$BASH_VERSION" ]; then
    echo "Error: This script must be run in Bash" >&2
    exit 1
fi

# Change the current working directory (cwd) to the same folder where the script
# is located. This ensures that relative paths within the script work as
# expected, regardless of where the script is invoked.
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 || exit

# Define default lists of features, packages, and PHP versions used in the
# installation process.
FEATURES_LIST=('ssl' 'ftp' 'dns' 'mail' 'awstats')
FEATURES_DEFAULT=('ssl' 'awstats')
FEATURES_POSSIBLE_VALUES=('all' 'none' "${FEATURES_LIST[@]}")

PACKAGES_LIST=('elasticsearch' 'manticore' 'virtualmin-syncthing' 'sysadmin' 'virtualmin-tikimanager' 'content-processing' 'r')
PACKAGES_LIST_STR=$(IFS=, ; echo "${PACKAGES_LIST[*]}")
PACKAGES_DEFAULT=('virtualmin-tikimanager' 'virtualmin-syncthing' 'sysadmin' 'content-processing') # same as wikisuite-virtualmin-all
PACKAGES_DEFAULT_STR=$(IFS=, ; echo "${PACKAGES_DEFAULT[*]}")
PACKAGES_BASE_LIST=('wikisuite-virtualmin-base') # from wikisuite-virtualmin-all, non optional
PACKAGES_POSSIBLE_VALUES=('all' "${PACKAGES_LIST[@]}")

PHP_VERSIONS_LIST=('5.6' '7.1' '7.2' '7.3' '7.4' '8.0' '8.1' '8.2' '8.3')
PHP_VERSIONS_LIST_STR=$(IFS=, ; echo "${PHP_VERSIONS_LIST[*]}")
PHP_VERSIONS_DEFAULT=('7.4' '8.1' '8.2' '8.3') # same as wikisuite-virtualmin-all
PHP_VERSIONS_DEFAULT_STR=$(IFS=, ; echo "${PHP_VERSIONS_DEFAULT[*]}")
PHP_VERSIONS_POSSIBLE_VALUES=('all' "${PHP_VERSIONS_LIST[@]}")

# Initialize default variables for processing command line arguments.
DEFAULT_WIKISUITE_APT_URL="https://packages.wikisuite.org"
DEFAULT_GITLAB_WIKISUITE_URL="https://gitlab.com/wikisuite/wikisuite-packages/-/raw/main"
DEFAULT_TIKI_COMPOSER_URL="http://composer.tiki.org"
WIKISUITE_APT_URL=${DEFAULT_WIKISUITE_APT_URL}
WIKISUITE_DEV_INSTALL_FROM_FOLDER=""
FEATURES=()
PACKAGES=()
PHP_VERSIONS=()

# Setup colors for messages
GREEN="" BLACK="" RED="" TURQUOISE="" RESET="" BOLD="" GRBG="" RDBG=""
if command -pv 'tput' > /dev/null; then
  GREEN=$(tput setaf 2)
  BLACK=$(tput setaf 0)
  RED=$(tput setaf 1)
  TURQUOISE=$(tput setaf 6)
  RESET=$(tput sgr0)
  BOLD=$(tput bold)
  GRBG=$(tput setab 22; tput setaf 10)
  RDBG=$(tput setab 52; tput setaf 9)
fi

# WikiSuite internal log file names
wikisuite_dry_run="$PWD/wikisuite-repos-test.log"

# Set variables for logging and tracking phases inherited by the Virtualmin
# installation script.
install_log_file_name=wikisuite-install-combined
export install_log_file_name
uninstall_log_file_name=wikisuite-uninstall
export uninstall_log_file_name
phases_total=6
export phases_total

# Internal function: log_ready
# Purpose: Removes ANSI escape sequences (color codes) from input for clean logging.
# Input: Accepts text with potential ANSI color codes.
# Output: Outputs plain text with color codes stripped.
log_ready() {
  sed -E 's/\x1B\[[0-9;]*[mK]//g; s/\x1B\([A-Za-z]//g'
}

# Function: check_value_in_array
# Purpose: 
#   Checks if a specific value exists in an array.
# Parameters:
#   $1 - The value to search for.
#   $@ - The array to search in (shifted by one).
# Returns: 
#   0 if the value is found, 1 otherwise.
check_value_in_array() {
  local VALUE=$1
  shift
  local ARRAY=("$@")
  local elem
  for elem in "${ARRAY[@]}" ; do
    if [ "$elem" == "$VALUE" ] ; then
      return 0
    fi
  done
  return 1
}

# Function: check_substr_in_array
# Purpose:
#   Checks if a specific substring is present in any element of an array.
# Parameters:
#   $1 - The substring to search for.
#   $@ - The array to search in (shifted by one).
# Returns:
#   0 if the substring is found in any element, 1 otherwise.
check_substr_in_array() {
  local VALUE=$1
  shift
  local ARRAY=("$@")
  local elem
  for elem in "${ARRAY[@]}" ; do
    if [[ "$elem" == *"$VALUE"* ]] ; then
      return 0
    fi
  done
  return 1
}
# Function: check_array_contains_array
# Purpose:
#   Checks if all elements of one array are contained within another array.
# Parameters:
#   $1 - The array to search in (haystack).
#   $2 - The array to search for (needles).
# Returns:
#   0 if all elements of the second array are found in the first array, 1 otherwise.
check_array_contains_array() {
  local tmp
  tmp="${1}[@]"
  local haystack=( "${!tmp}" )
  tmp="${2}[@]"
  local needles=( "${!tmp}" )
  local n
  for n in "${needles[@]}" ; do
    if ! check_value_in_array "$n" "${haystack[@]}" ; then
      return 1
    fi
  done
  return 0
}

# Function: wiki_conf_repos
# Purpose:
#   Configures repositories for WikiSuite installation.
#   Handles two cases: using a local folder for development/testing purposes or 
#   using the standard repository URL for production installations.
# Behavior:
#   - Installs the GPG key from either a local folder or the WikiSuite repository URL.
#   - Sets up the appropriate APT repository based on the system architecture.
wiki_conf_repos() {
  if [ -n "${WIKISUITE_DEV_INSTALL_FROM_FOLDER}" ] ; then
    # Configure WikiSuite to use a local folder as source, used to test/validate changes without merge to master
    # Download the artificats from the generatePages job and unzip them in a folder on the server
    echo "Warning: Installing WikiSuite from a local folder (${WIKISUITE_DEV_INSTALL_FROM_FOLDER}) for tests"
    gpg --no-default-keyring --dearmor < "${WIKISUITE_DEV_INSTALL_FROM_FOLDER}/GPG_PUBLIC_KEY" > /etc/apt/trusted.gpg.d/wikisuite-packages.gpg
    # shellcheck disable=SC2154
    echo "deb file:${WIKISUITE_DEV_INSTALL_FROM_FOLDER}/${os_type} $(lsb_release -sc) main" > /etc/apt/sources.list.d/wikisuite.list
  else
    # Normal - production - usage, we will install from https://packages.wikisuite.org
    # You can use -d to set a different url
    if [ "${WIKISUITE_APT_URL}" != "${DEFAULT_WIKISUITE_APT_URL}" ] ; then
      echo "Warning: Installing WikiSuite from a custom url (${WIKISUITE_APT_URL}) for tests"
    fi
    curl -s -S -L "${WIKISUITE_APT_URL}/GPG_PUBLIC_KEY" | gpg --no-default-keyring --dearmor > /etc/apt/trusted.gpg.d/wikisuite-packages.gpg
    # GitLab PPA generates 'binary-amd64' repo, when should produce 'binary-all', so for now
    # we need to force the architecture as a workaround to make it work on ARM processors
    if [ "$(uname -m)" != "x86_64" ] ; then
      repo_arch_force=' [arch=amd64]'
    fi
    echo "Setting up main WikiSuite repository /etc/apt/sources.list.d/wikisuite.list"
    echo "deb$repo_arch_force ${WIKISUITE_APT_URL}/${os_type} $(lsb_release -sc) main" > /etc/apt/sources.list.d/wikisuite.list
  fi
  # Update the repositories
  echo "Cleaning and updating the repositories cache"
  eval "$update"
}

# Function: wiki_packages_prepare
# Purpose:
#   Prepares the list of packages and repositories to install based on user-selected configurations.
#   Packages and repositories are prepared differently based on the value of `return_type`.
# Behavior:
#   - Handles checks for optional package lists and version-specific packages.
#   - Builds separate lists for packages and repositories.
wiki_packages_prepare() {
  # decide what repositories and packages to install
  local return_type="$1"
  local PKG_INSTALL_LIST=()
  local INSTALL_WIKISUITE_ALL=1
  local REPO_INSTALL_LIST=()
  if check_array_contains_array PACKAGES PACKAGES_DEFAULT && check_array_contains_array PHP_VERSIONS PHP_VERSIONS_DEFAULT ; then
    INSTALL_WIKISUITE_ALL=0
    PKG_INSTALL_LIST+=('wikisuite-virtualmin-all')
  fi
  if [ ! ${INSTALL_WIKISUITE_ALL} ] ; then
    PKG_INSTALL_LIST+=("${PACKAGES_BASE_LIST[@]}")
  fi
  for p in "${PACKAGES_LIST[@]}" ; do
    [ ${INSTALL_WIKISUITE_ALL} -eq 0 ] && check_value_in_array "$p" "${PACKAGES_DEFAULT[@]}" && continue
    if check_value_in_array "$p" "${PACKAGES[@]}" ; then
      PKG_INSTALL_LIST+=( "wikisuite-$p" )
    fi
  done
  for p in "${PHP_VERSIONS_LIST[@]}" ; do
    [ ${INSTALL_WIKISUITE_ALL} -eq 0 ] && check_value_in_array "$p" "${PHP_VERSIONS_DEFAULT[@]}" && continue
    if check_value_in_array "$p" "${PHP_VERSIONS[@]}" ; then
      PKG_INSTALL_LIST+=( "wikisuite-php$p" )
    fi
  done
  
  REPO_INSTALL_LIST=()
  if [ ${INSTALL_WIKISUITE_ALL} -eq 0 ] ; then
    REPO_INSTALL_LIST+=('wikisuite-external-repositories')
  else
    REPO_INSTALL_LIST+=('wikisuite-external-repo-nodejs') # is not linked to optional packages
    check_substr_in_array '-elasticsearch' "${PKG_INSTALL_LIST[@]}" && REPO_INSTALL_LIST+=('wikisuite-external-repo-elasticsearch')
    check_substr_in_array '-manticore' "${PKG_INSTALL_LIST[@]}" && REPO_INSTALL_LIST+=('wikisuite-external-repo-manticore')
    check_substr_in_array '-php' "${PKG_INSTALL_LIST[@]}" && REPO_INSTALL_LIST+=('wikisuite-external-repo-php')
    check_substr_in_array '-syncthing' "${PKG_INSTALL_LIST[@]}" && REPO_INSTALL_LIST+=('wikisuite-external-repo-syncthing')
  fi
  # Return based on the parameter provided
  case "$return_type" in
    repos)
      echo "${REPO_INSTALL_LIST[*]}"   # Return only repositories
      ;;
    packages)
      echo "${PKG_INSTALL_LIST[*]}"    # Return only packages
      ;;
    *)
      echo "Invalid return type: $return_type. Use 'repos' or 'packages'."
      return 1
      ;;
  esac
}

# Function: wiki_repos
# Purpose:
#   Installs the prepared repository list for WikiSuite.
wiki_repos() {
  local repo_list
  repo_list=$(wiki_packages_prepare repos)
  echo "Updating the repositories cache"
  eval "$update"
  echo "Installing the following repositories: $repo_list"
  eval "$install $repo_list"
}

# Function: wiki_packages
# Purpose:
#   Installs the prepared package list for WikiSuite.
wiki_packages() {
  local pkg_list
  pkg_list=$(wiki_packages_prepare packages)
  echo "Updating the repositories cache"
  eval "$update"
  echo "Installing the following packages: $pkg_list"
  eval "$install $pkg_list"
}

# Function: wiki_configure_virtualmin_features
# Purpose:
#   Configures features and associated services in Virtualmin during setup.
#   Each feature may have an associated service that is enabled/disabled as part of the process.
wiki_configure_virtualmin_features() {
  local action=$1
  # Internal function: manage_virtualmin_feature
  # Purpose: Enable or disable a specific feature in Virtualmin.
  # Parameters:
  #   $1 - The action to perform ("enable" or "disable").
  #   $2 - The feature to manage (e.g., "ssl", "mail").
  # Behavior:
  #   Uses Virtualmin's command-line interface to enable or disable the feature.
  # Returns:
  #   0 if the feature is successfully enabled or disabled, 1 otherwise.

  manage_virtualmin_feature() {
    local action=$1
    local feature=$2
    if [ "$action" == "enabled" ]; then
      virtualmin set-global-feature --enable-feature "${feature}" --default-on "${feature}" && return 0 || return 1
    else
      virtualmin set-global-feature --default-off "${feature}" --disable-feature "${feature}" && return 0 || return 1
    fi
  }
  # Internal function: manage_service
  # Purpose: Enable or disable a service using systemd (or SysVinit if applicable).
  # Parameters:
  #   $1 - The action to perform ("enable" or "disable").
  #   $2 - The services to manage (one or more service names, separated by spaces).
  # Behavior:
  #   Checks if the service exists and enables or disables it using systemd.
  #   If the service is not found, the function returns success (non-blocking).
  # Returns:
  #   0 if all services succeed, 1 if any service fails.

  manage_service() {
    local action=$1
    local services=$2
    for service in $services; do
      if systemctl list-unit-files --type=service --all | grep -q "^${service}\.service"; then
        if [ "$action" == "enabled" ]; then
          systemctl enable --now "${service}.service" && echo "Service ${service} enabled" || return 1
        else
          systemctl disable --now "${service}.service" && echo "Service ${service} disabled" || return 1
        fi
      else
        return 0  # Service does not exist, return success
      fi
    done
    return 0  # Return success if all services succeed
  }
  # Internal function: feature_handler
  # Purpose: Handles both Virtualmin features and associated services based on
  # the action provided.
  # Parameters:
  #   $1 - The action to perform ("enable" or "disable").
  #   $2 - The feature to manage (e.g., "ssl", "mail").
  #   $3 - (Optional) The services associated with the feature, if any.
  # Behavior:
  #   This function handles enabling or disabling both Virtualmin features
  #   and services. It first manages the feature, and if a service is provided,
  #   it also enables or disables the service.
  # Returns:
  #   0 if both the feature and service management succeed, 1 otherwise.
  feature_handler() {
    local action=$1
    local feature=$2
    local service=$3
    local vena="Enabling"
    local vdis="Disabling"
    local tell
    tell=$([[ "$action" == "enabled" ]] && echo "$vena" || echo "$vdis")
    echo " ◻ ${tell} feature ${feature} in Virtualmin"
    # Call manage_virtualmin_feature and propagate its return status
    manage_virtualmin_feature "$action" "$feature" || return 1

    # If service exists, process it as well
    if [ -n "${service}" ]; then
      echo " ◻ ${tell} service ${service}"
      manage_service "$action" "${service}" || return 1
    fi

    return 0  # Return success if both feature and service management succeed
  }

  # Handle the feature logic based on the feature type
  case $2 in
    "ssl")
      local feature="ssl"
      [[ "${bundle}" == *LEMP* ]] && feature="virtualmin-nginx-ssl"
      feature_handler "$action" "$feature" || return 1
      ;;
    "mail")
      feature_handler "$action" "mail" "dovecot.service postfix.service" || return 1
      feature_handler "$action" "spam" "spamassassin spamd" || return 1
      feature_handler "$action" "virus" "clamav-freshclam clamav-daemon" || return 1
      ;;
    "ftp")
      feature_handler "$action" "ftp" "proftpd.service" || return 1
      ;;
    "dns")
      feature_handler "$action" "dns" "named.service" || return 1
      ;;
    "awstats")
      feature_handler "$action" "virtualmin-awstats" || return 1
      ;;
  esac
  return 0  # Return success if all feature handling is successful
}

# Internal function: wiki_configure_etckeeper
# Purpose: Configure Etckeeper custom script to run before and after domain
# modifications
# Returns:
#   0 if the configuration succeeds, 1 otherwise
wiki_configure_etckeeper() {
  local file="etckeeper-virtual-server-tracker.sh"
  local url="${DEFAULT_GITLAB_WIKISUITE_URL}/$file"
  local filepath="/etc/webmin/virtual-server/$file"

  # Download the file
  if ! download_remote "$url" "$filepath"; then
    return 1
  fi

  # Make the file executable
  if ! chmod +x "$filepath"; then
    echo "Failed to make \"$filepath\" executable" >&2
    return 1
  fi

  # Configure Webmin to use the script as pre_command and post_command
  local opts
  opts=(pre post)
  for a in "${opts[@]}"; do
     cmd="webmin set-config -m virtual-server -o ${a}_command -v '$filepath --${a}'"
     eval "$cmd"
  done

  return 0
}

# Internal function: test_connection
# Purpose: Tests basic connectivity to a domain using a specific IP protocol.
# Parameters:
#   $1 - The URL or domain to test
#   $2 - The IP protocol to use ("ipv4" or "ipv6")
# Returns:
#   0 if the connection succeeds, 1 otherwise
test_connection() {
  local input="$1"
  local ip_version="$2"
  local ip_version_nice
  ip_version_nice=${ip_version/ip/IP}
  local timeout=5
  local domain
  local http_protocol="http"
  local http_protocol_nice
  http_protocol_nice=$(echo "$http_protocol" | tr '[:lower:]' '[:upper:]')

  # Extract the domain from the input
  domain=$(echo "$input" | awk -F[/:] '{print $4}')
  [ -z "$domain" ] && domain="$input"

  # Validate parameters
  if [ -z "$domain" ] || [ -z "$ip_version" ]; then
    echo "${RED}[ERROR]  ${RESET} Domain and IP version are required" >&2
    return 1
  fi

  # Setup protocol-specific flags
  local ping_cmd http_cmd
  case "$ip_version" in
    ipv4)
      if ! getent ahostsv4 "$domain" >/dev/null 2>&1; then
        echo "${RED}[ERROR]  ${RESET} ${BOLD}$domain${RESET} — cannot find IPv4 address" >&2
        return 1
      fi
      ping_cmd="ping -c 1 -W $timeout $domain"
      http_cmd="curl -sS --ipv4 --max-time $timeout --head $http_protocol://$domain \
        || wget --spider -4 -T $timeout $http_protocol://$domain"
      ;;
    ipv6)
      if ! getent ahostsv6 "$domain" >/dev/null 2>&1; then
        echo "${RED}[ERROR]  ${RESET} ${BOLD}$domain${RESET} — cannot find IPv6 address" >&2
        return 1
      fi
      ping_cmd="ping6 -c 1 -W $timeout $domain"
      http_cmd="curl -sS --ipv6 --max-time $timeout --head $http_protocol://$domain \
        || wget --spider -6 -T $timeout $http_protocol://$domain"
      ;;
  esac

  # Try ping first
  if eval "$ping_cmd" >/dev/null 2>&1; then
    echo "${GREEN}[SUCCESS]${RESET} ${GRBG}[$ip_version_nice]${RESET} ${GRBG}[ICMP]${RESET} ${BOLD}$domain${RESET}"
  else
    echo "${RED}[ERROR]  ${RESET} ${RDBG}[$ip_version_nice]${RESET} ${RDBG}[ICMP]${RESET} ${BOLD}$domain${RESET}"
  fi

  # HTTP test as well
  if command -v 'curl' > /dev/null || command -v 'wget' > /dev/null; then
    if eval "$http_cmd" >/dev/null 2>&1; then
      echo "${GREEN}[SUCCESS]${RESET} ${GRBG}[$ip_version_nice]${RESET} ${GRBG}[$http_protocol_nice]${RESET} ${BOLD}$domain${RESET}"
      return 0
    else
      echo "${RED}[ERROR]  ${RESET} ${RDBG}[$ip_version_nice]${RESET} ${RDBG}[$http_protocol_nice]${RESET} ${BOLD}$domain${RESET}"
      return 1
    fi
  fi
}

# Internal function: dry_run_apt
# Purpose: Tests the connectivity of APT repositories and captures errors and
# warnings.
# Parameters:
#   $1 - The repository definition
#   $2 - Optional parameters like architecture
# Returns:
#   0 if the APT test succeeds, 1 otherwise
dry_run_apt() {
  local repo="$1"   # Repository definition
  local params="$2" # Optional params like architecture

  # Read OS information from /etc/os-release
  if [ -f /etc/os-release ]; then
    # shellcheck disable=SC1091
    . /etc/os-release
    OS_NAME=$ID
    OS_CODENAME=$VERSION_CODENAME
  else
    echo -e "${RED}[ERROR]  ${RESET} Could not read OS information in /etc/os-release" >&2
    return 1
  fi

  # Replace dynamic variables in the repository definition
  local repo_processed
  repo_processed=$(echo "$repo" | sed "s/\\\$ID/$OS_NAME/g" | \
    sed "s/\\\$VERSION_CODENAME/$OS_CODENAME/g")

  # Extract URL, branch, and component
  local repo_url
  local repo_branch
  local repo_component
  repo_url=$(echo "$repo_processed" | awk '{print $1}')
  repo_branch=$(echo "$repo_processed" | awk '{print $2}')
  repo_component=$(echo "$repo_processed" | awk '{print $3}')

  # Extract protocol from the URL
  local repo_protocol
  repo_protocol=$(echo "$repo_url" | awk -F: '{print $1}' | \
    tr '[:lower:]' '[:upper:]')

  # Extract the domain from the URL
  local repo_domain
  repo_domain=$(echo "$repo_url" | awk -F/ '{print $3}')

  # Build the APT repository line
  local apt_line="deb [trusted=yes signed-by=/dev/null"
  if [ -n "$params" ]; then
    apt_line="$apt_line $params"
  fi
  apt_line="$apt_line] $repo_url $repo_branch $repo_component"

  # Generate temporary sources file
  local temp_sources
  temp_sources="/tmp/temp-repo-$(echo "$repo_url" | md5sum | \
    cut -d' ' -f1).list"
  echo "$apt_line" > "$temp_sources"

  # Print to log repo line
  echo "Testing APT repository:" >> "$wikisuite_dry_run"
  echo "  $apt_line" >> "$wikisuite_dry_run"

  # Invalidate all cached repository metadata
  rm -rf /var/lib/apt/lists/*

  # Run apt-get update with the temporary sources list and capture output
  local apt_output
  apt_output=$(apt-get -o Dir::Etc::sourcelist="$temp_sources" \
                       -o Dir::Etc::sourceparts="/dev/null" \
                       -o Acquire::Check-Valid-Until=false \
                       -o Acquire::Check-Signature=false \
                       -o Acquire::AllowInsecureRepositories=true \
                       -o Acquire::AllowDowngradeToInsecureRepositories=true \
                       -o APT::Get::AllowUnauthenticated=true \
                        update 2>&1 | tee >(log_ready >> "$wikisuite_dry_run"))

  # Parse the output for errors or warnings
  if echo "$apt_output" | grep -qE "Err:|E:"; then
    # Extract error details
    local error_msg
    error_msg=$(echo "$apt_output" | grep -A 1 "Err:" | sed -n '2s/^[ \t]*//p')
    if [ -z "$error_msg" ]; then
        error_msg=$(echo "$apt_output" | grep "^E:" | sed 's/^E:[ \t]*//')
    fi
    # Get terminal width and truncate error_msg
    local term_width max_length
    term_width=$(tput cols || echo 80)
    [ "$term_width" -lt 80 ] && term_width=80
    max_length=$((term_width - 27))
    max_length=$((max_length - ${#repo_domain}))
    if [ ${#error_msg} -gt $max_length ]; then
        error_msg=$(echo "$error_msg" | cut -c1-"$max_length")...
    fi
    # Print error message
    echo -e "${RED}[ERROR]  ${RESET} ${RDBG}[$repo_protocol]${RESET} ${repo_domain} : ${error_msg}" | \
      tee >(log_ready >> "$wikisuite_dry_run") >&2
    rm -f "$temp_sources"
    return 1
  fi

  # Handle non-critical warnings separately
  echo "$apt_output" | grep -E "^(N:|W:)" | while read -r warning; do
    warning=$(echo "$warning" | tr -d '\n')
    echo "$warning" >> "$wikisuite_dry_run"
  done

  # If no errors, print success
  echo -e "${GREEN}[SUCCESS]${RESET} ${GRBG}[$repo_protocol]${RESET} $repo_domain" | \
    tee >(log_ready >> "$wikisuite_dry_run")

  # Cleanup temporary sources file
  rm -f "$temp_sources"
  return 0
}

# Register Virtualmin hooks for the installer
hook__usage() {
    split_text_with_indent() {
        local text="$1"
        local max_length="$2"
        local first_line_indent="$3"
        local subsequent_line_indent="$4"
        local line=""

        # Print the first part of the line with proper indentation
        printf "%${first_line_indent}s" ""

        if [[ "$text" == *":"* ]]; then
            local prefix="${text%%:*}:"
            printf " %s" "$prefix"  # Added an extra space before the prefix
            text="${text#*:}"
            text="${text#"${text%%[![:space:]]*}"}" # Trim leading spaces
            
            # Move to the next line and indent
            printf "\n%${subsequent_line_indent}s" ""
        fi

        while [ "${#text}" -gt 0 ]; do
            local current_indent=$subsequent_line_indent
            local available_space=$((max_length - current_indent))
            
            if [ "${#text}" -le "$available_space" ]; then
                printf "%s\n" "$text"
                break
            fi

            line="${text:0:$available_space}"
            line="${line% *}" # Break at the last space
            printf "%s\n" "$line"

            text="${text:${#line}}"
            text="${text#"${text%%[![:space:]]*}"}" # Trim leading spaces
            
            if [ -n "$text" ]; then
                printf "%${current_indent}s" ""
            fi
        done
    }

    # Maximum length for each line
    local max_line_length=84
    local def_line_indent=32
    local first_line_indent=34
    local subsequent_line_indent=35

    echo
    printf "Usage: %s [options]\\n" "$(basename "$0")"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--bundle|-b <LAMP|LEMP>" "choose bundle to install (defaults to LAMP)"
    printf "  %-${def_line_indent}s %s\\n" "--minimal|-m" "install a minimal package set for low-memory systems"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--hostname|-n" "force hostname during installation"
    printf "%-${subsequent_line_indent}s%s\\n" "" "current: $(hostname)"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--no-package-updates|-x" "skip package updates during installation"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--insecure-downloads|-i" "skip remote server SSL certificate check upon downloads"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--features|-ft <FEATURES>" "comma-separated list of features to install"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--packages|-pk <PACKAGES>" "comma-separated list of packages to install"
    split_text_with_indent "available packages: [${PACKAGES_LIST_STR[*]}] or 'all'; default: ${PACKAGES_DEFAULT_STR[*]}" "$max_line_length" "$first_line_indent" "$subsequent_line_indent"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--php-versions|-php <VERSIONS>" "comma-separated list of PHP versions to install"
    split_text_with_indent "available PHP versions: [${PHP_VERSIONS_LIST_STR[*]}] or 'all'; default: ${PHP_VERSIONS_DEFAULT_STR[*]}" "$max_line_length" "$first_line_indent" "$subsequent_line_indent"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--apt-url|-d <URL>" "use the URL provided instead of https://packages.wikisuite.org as"
    printf "%-${subsequent_line_indent}s%s\\n" "" "the source repository for WikiSuite packages, useful for testing"
    printf "  %-${def_line_indent}s %s\\n" "--repository-folder|-r <FOLDER>" "use a folder as the repository in APT for WikiSuite packages"
    printf "%-${subsequent_line_indent}s%s\\n" "" "instead of the website, useful for testing"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--test-conn|-T <ipv4|ipv6>" "test connectivity to the repositories and exit"
    printf "  %-${def_line_indent}s %s\\n" "--dry-run-apt|-D" "test the APT repositories connectivity and exit"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--uninstall|-u" "remove all WikiSuite and Virtualmin packages and dependencies"
    echo
    printf "  %-${def_line_indent}s %s\\n" "--force|-f|--yes|-y" "assume \"yes\" as answer to all prompts"
    printf "  %-${def_line_indent}s %s\\n" "--force-reinstall|-fr" "force reinstallation (not recommended)"
    printf "  %-${def_line_indent}s %s\\n" "--no-banner|-nb" "suppress installation messages and warnings"
    printf "  %-${def_line_indent}s %s\\n" "--verbose|-v" "increase verbosity"
    printf "  %-${def_line_indent}s %s\\n" "--version|-V" "show installer version"
    printf "  %-${def_line_indent}s %s\\n" "--help|-h" "assume \"yes\" to all prompts"
    echo
}

hook__show_version() {
  echo "$TVER"
  exit 0
}

hook__parse_args() {
  while [ "$1" != "" ]; do
    case $1 in
      # Primary Arguments (names unchanged)
      --help | -h)
        bind_hook "usage"
        exit 0
        ;;
      --bundle | -b)
        shift
        case "$1" in
          LAMP)
            shift
            bundle='LAMP'
            ;;
          LEMP)
            shift
            bundle='LEMP'
            ;;
          *)
            printf "Unknown bundle %s: exiting\n" "$1"
            exit 1
            ;;
        esac
        ;;
      --minimal | -m)
        mode='minimal'
        shift
        ;;
      --insecure-downloads | -i)
        insecure_download_wget_flag=' --no-check-certificate'
        insecure_download_curl_flag=' -k'
        shift
        ;;
      --no-package-updates | --disable-updates | -x)
        noupdates=1
        shift
        ;;
      --setup | -s)
        setup_only=1
        shift
        mode='setup'
        unstable='unstable'
        log_file_name="${setup_log_file_name:-virtualmin-repos-setup}"
        ;;
      --unstable | -e)
        shift
        unstable='unstable'
        virtualmin_config_system_excludes=""
        virtualmin_stack_custom_packages=""
        ;;
      --module | -o)
        shift
        module_name=$1
        shift
        ;;
      --hostname | -n | -H)
        shift
        forcehostname=$1
        HOST=$1
        shift
        ;;
      --force | -f | --yes | -y)
        shift
        skipyesno=1
        ;;
      --force-reinstall | -fr)
        shift
        forcereinstall=1
        ;;
      --no-banner | -nb)
        shift
        skipbanner=1
        ;;
      --verbose | -v)
        shift
        VERBOSE=1
        ;;
      --version | -V)
        shift
        showversion=1
        ;;
      --uninstall | -u)
        shift
        mode="uninstall"
        log_file_name="${uninstall_log_file_name:-virtualmin-uninstall}"
        ;;
      --features | -ft)
        shift
        IFS=', ' read -r -a LIST <<< "$1"
        for i in "${LIST[@]}" ; do
          if ! check_value_in_array "$i" "${FEATURES_POSSIBLE_VALUES[@]}" ; then
            printf "Feature \"%s\" is not recognized.\nKnown features are:\n  %s\n" "$i" "${FEATURES_POSSIBLE_VALUES[*]}"
            bind_hook "usage"
            exit 1
          fi
          if [ "$i" = "all" ] ; then
            FEATURES+=("${FEATURES_LIST[@]}")
          else
            FEATURES+=("$i")
          fi
        done
        shift
        ;;
      --packages | -pk)
        shift
        IFS=', ' read -r -a LIST <<< "$1"
        for i in "${LIST[@]}" ; do
          if ! check_value_in_array "$i" "${PACKAGES_POSSIBLE_VALUES[@]}" ; then
            printf "Package name \"%s\" is not recognized.\nKnown packages are:\n  %s\n" "$i" "${PACKAGES_POSSIBLE_VALUES[*]}"
            bind_hook "usage"
            exit 1
          fi
          if [ "$i" = "all" ] ; then
            PACKAGES+=("${PACKAGES_LIST[@]}")
          else
            PACKAGES+=("$i")
          fi
        done
        shift
        ;;
      --php-versions | -php)
        shift
        IFS=', ' read -r -a LIST <<< "$1"
        for i in "${LIST[@]}" ; do
          if ! check_value_in_array "$i" "${PHP_VERSIONS_POSSIBLE_VALUES[@]}" ; then
            printf "PHP %s is not a recognized version.\nKnown versions are:\n  %s\n" "$i" "${PHP_VERSIONS_POSSIBLE_VALUES[*]}"
            bind_hook "usage"
            exit 1
          fi
          if [ "$i" = "all" ] ; then
            PHP_VERSIONS+=("${PHP_VERSIONS_LIST[@]}")
          else
            PHP_VERSIONS+=("$i")
          fi
        done
        shift
        ;;
      --repository-folder | -r)
        shift
        WIKISUITE_DEV_INSTALL_FROM_FOLDER=$1
        if [ ! -d "${WIKISUITE_DEV_INSTALL_FROM_FOLDER}" ] ; then
          echo "The packages root folder does not exist"
          bind_hook "usage"
          exit 1
        fi
        if [ ! -f "${WIKISUITE_DEV_INSTALL_FROM_FOLDER}/GPG_PUBLIC_KEY" ] ; then
          echo "The packages folder provided does not have the right content/format"
          bind_hook "usage"
          exit 1
        fi
        shift
        ;;
      --apt-url | -d)
        shift
        WIKISUITE_APT_URL=$1
        if [ -z "${WIKISUITE_APT_URL}" ]; then
          echo "The URL provided can't be empty"
          bind_hook "usage"
          exit 1
        fi
        if case "${WIKISUITE_APT_URL}" in https://*) false;; http://*) false;; *) true;; esac; then
          echo "The url provided (${WIKISUITE_APT_URL}) does not start with http:// or https://"
          bind_hook "usage"
          exit 1
        fi
        shift
        ;;
      
      # Catch-all for unrecognized options
      *)
        printf "Unrecognized option: %s\n" "$1"
        bind_hook "usage"
        exit 1
        ;;
    esac
  done

  # Set default params unless already set
  if [ ${#FEATURES[@]} -eq 0 ]; then
    FEATURES=("${FEATURES_DEFAULT[@]}")
  fi
  if [ ${#PACKAGES[@]} -eq 0 ]; then
      PACKAGES=("${PACKAGES_DEFAULT[@]}")
  fi
  if [ ${#PHP_VERSIONS[@]} -eq 0 ]; then
      PHP_VERSIONS=("${PHP_VERSIONS_DEFAULT[@]}")
  fi

  # Toggle dependent options based on the selected features,
  # unless enforced by the user from the command line
  if ! check_value_in_array 'mail' "${FEATURES[@]}" && [ "$mode" = 'full' ]; then
    mode='minimal'
  fi
}

hook__uninstall() {
  # Show warning and ask for confirmation unless forced
  if [ -z "$this__skipyesno" ]; then
    echo
    # shellcheck disable=SC2153
    printf "  %sWARNING%s\\n" "$REDBG" "$NORMAL"
    echo
    echo "  This operation is highly disruptive and cannot be undone. It removes all"
    echo "  of the packages and configuration files installed by the WikiSuite and"
    echo "  Virtualmin installers."
    echo
    echo "  It must never be executed on a live production system."
    printf " %sUninstall?%s (y/N) " "$RED" "$NORMAL"
    skipyesno=0
    if ! yesno; then
      exit 0
    fi
    skipyesno=1
  fi
  # Always sleep just a bit in case the user changes their mind
  sleep 3
  # Define uninstallation commands and functions
  local uninstall_cmd="apt-get remove --assume-yes --purge"
  local uninstall_cmd_auto="apt-get autoremove --assume-yes"
  # shellcheck disable=SC2317
  wikisuite_uninstall()
  {
    local un_packages_all="wikisuite.*"
    log_info "Uninstall command \"$uninstall_cmd\""
    for package in $un_packages_all; do
      log_info "Uninstalling package \"$package\""
      # shellcheck disable=SC2086
      local run="$uninstall_cmd \"$package\" || true"
      echo "Running uninstallation command \"$run\""
      eval "$run"
    done
  }
  # shellcheck disable=SC2317
  wikisuite_uninstall_related()
  {
    local un_packages_all="php.*"
    local un_packages_list_str="${PACKAGES_LIST_STR[*]//,/ }"
    local un_packages_default_str="${PACKAGES_DEFAULT_STR[*]//,/ }"
    local all_packages="$un_packages_all $un_packages_list_str $un_packages_default_str"
    for package in $all_packages; do
      log_info "Uninstalling package \"$package\""
      # shellcheck disable=SC2086
      local run="$uninstall_cmd \"$package\" || true"
      echo "Running uninstallation command \"$run\""
      eval "$run"
    done
  }
  # shellcheck disable=SC2317
  wikisuite_uninstall_deps()
  {
    log_info "Uninstall auto-remove command \"$uninstall_cmd_auto\""
    local run="$uninstall_cmd_auto"
    echo "Running auto-remove command \"$uninstall_cmd_auto\""
    eval "$run"
  }
  # Number of phases
  phases_total=2
  # Uninstall WikiSuite first
  echo ""
  phase "Uninstalling WikiSuite" 1
  run_ok "wikisuite_uninstall" "Uninstalling WikiSuite packages"
  run_ok "wikisuite_uninstall_related" "Uninstalling WikiSuite related packages"
  run_ok "wikisuite_uninstall_deps" "Uninstalling WikiSuite dependencies"
  # Uninstall Virtualmin using original uninstall function
  phase_number=2
  uninstall_phase_description="Uninstalling Virtualmin"
  uninstall
}

hook__install_msg() {
  # If defined, it will override the default welcome message
  cat <<EOF

  Welcome to ${CYANBG}${BLACK}${BOLD} WikiSuite ${NORMAL} installer with ${GREENBG}${BLACK}${BOLD} Virtualmin $PRODUCT ${NORMAL} stack, version $TVER

  This script must be run on a freshly installed supported OS. It does not
  perform updates or upgrades (use your system package manager).

  The systems currently supported by the install script are:

    ${CYANBG}${BLACK}${BOLD}Debian Linux and derivatives${NORMAL}${CYAN}
        - Debian 10, 11 and 12 on i386, amd64 and arm64
        - Ubuntu 20.04 LTS, 22.04 LTS and 24.04 LTS on i386, amd64 and arm64${NORMAL}

  If your OS is not listed, installation ${BOLD}${RED}will fail${NORMAL}.

  The selected package bundle is ${GREEN}${bundle}${NORMAL} and the size of install is
  ${GREEN}${mode}${NORMAL}. It will require up to ${GREEN}${disk_space_required} GB${NORMAL} of disk space.

EOF

  if [ "$skipyesno" -ne 1 ]; then
    printf " Continue? (y/n) "
    if ! yesno; then
      exit
    fi
  fi
}

hook__preconfigured_system_msg() { 
  # If defined, it will override the default system message about
  # pre-installed software
  # Double check if installed, just in case above error ignored.
  is_preconfigured_rs=$(is_preconfigured)
  if [ -n "$is_preconfigured_rs" ]; then
    cat <<EOF

  ${WHITEBG}${RED}${BOLD} ATTENTION ${NORMAL}

  Pre-installed software detected: $is_preconfigured_rs

  It is highly advised ${BOLD}${RED}not to pre-install${NORMAL} any additional packages on your
  OS. The installer expects a freshly installed OS, and anything you do
  differently might cause conflicts or configuration errors. If you need
  to enable third-party package repositories, do so after installation
  and only with extreme caution.

EOF
    if [ "$skipyesno" -ne 1 ]; then
      printf " Continue? (y/n) "
      if ! yesno; then
        exit
      fi
    fi
  fi
}

hook__already_installed_block() {
  # If defined, it will override the default block message about
  # installed Virtualmin
  log_error "Your system already has a successful WikiSuite and Virtualmin"
  log_error "installed. Re-installation is neither possible nor necessary."
  log_error "This script must be run on a freshly installed supported"
  log_error "operating system."
  exit 100
}

hook__phase3_post() {
  # If defined, it will run after the default phase 3

  # Install essential packages if not installed
  local missingPackages=();
  if [ ! -x "/usr/bin/curl" ]; then
    missingPackages+=('curl')
  fi

  if [ ! -x "/usr/bin/wget" ]; then
    missingPackages+=('wget')
  fi

  if [ ! -x "/usr/bin/sudo" ]; then
    missingPackages+=('sudo')
  fi

  if [ ! -x "/bin/ps" ]; then
    missingPackages+=('procps')
  fi

  if [ ! -x "/usr/bin/lsb_release" ]; then
    missingPackages+=('lsb-release')
  fi

  if [ ! -x "/usr/lib/apt/methods/https" ]; then
    missingPackages+=('apt-transport-https')
  fi

  if [ ! -x "/usr/bin/gpg" ]; then
    missingPackages+=('gnupg')
  fi

  if (( ${#missingPackages[@]} != 0 )); then
    run_ok "$install ${missingPackages[*]}" "Installing additional required packages"
  fi
}

hook__phase4_pre() {
  # If defined, it will run before the default phase 4
  # Pre-disable DNS globally before running Virtualmin configuration
  # stage only if DNS is not selected
  if ! check_value_in_array 'dns' "${FEATURES[@]}" ; then
    webmin set-config -m virtual-server -o dns -v 0 -f
  fi
}

hook__post_install_message() {
  # If defined, it will override the default post-install message
  log_success "WikiSuite installation with Virtualmin $PRODUCT stack is complete."
  log_success "If there were no errors above, it should be ready to configure"
  log_success "at https://${hostname}:10000 (or https://${address}:10000)."
  if [ -z "$ssl_host_success" ]; then
    log_success "You may receive a security warning in your browser on your first visit."
  fi
}

# Validate Host input
if [ -z "$HOST" ]; then
  HOST=$(hostname)
fi
export WIKISUITE_INSTALL_HOST="${HOST}"

# It will run after the default phases to add additional stages and their
# commands with descriptions
hooks__phases='
5	Installation of WikiSuite	wiki_conf_repos	Configuring WikiSuite repositories
5	Installation of WikiSuite	wiki_repos	Installing WikiSuite external repositories
5	Installation of WikiSuite	wiki_packages	Installing WikiSuite packages
5	Installation of WikiSuite	wiki_configure_etckeeper	Setting up Etckeeper for Virtualmin
'
export hooks__phases

hook__phases_all_post() {
  echo
  phase "Configuration of WikiSuite" 6
  local total=${#FEATURES_LIST[@]}
  local count=0
  run_errors_fatal_local=$RUN_ERRORS_FATAL
  unset RUN_ERRORS_FATAL
  for F in "${FEATURES_LIST[@]}"; do
    count=$((count + 1))
    local action="disabled"
    if check_value_in_array "$F" "${FEATURES[@]}" ; then
      action="enabled"
    fi
    local message="[${YELLOW}${count}${NORMAL}/${GREEN}${total}${NORMAL}]"
    declare -A FEATURE_DISPLAY_MAP=(
      ["dns"]="DNS"
      ["ssl"]="SSL"
      ["ftp"]="FTP"
      ["awstats"]="AWStats"
      ["mail"]="Mail"
    )
    message="${message} Configuring ${FEATURE_DISPLAY_MAP[$F]} to be ${action}"
    spaces_to_add=$((87 - ${#message}))
    for ((i=0; i<spaces_to_add; i++)); do
      message="${message} "
    done
    run_ok "wiki_configure_virtualmin_features $action $F" "$message" 2>&1
  done
  RUN_ERRORS_FATAL=$run_errors_fatal_local
}

# Cleanup downloaded files after the installation
hook__clean_post() {
  cleanup_downloads
}

# Default URLs for Virtualmin installation scripts
DEFAULT_VIRTUALMIN_URL=${DEFAULT_VIRTUALMIN_URL:-https://software.virtualmin.com}
DEFAULT_VIRTUALMIN_SCRIPTS_URL=${DEFAULT_VIRTUALMIN_SCRIPTS_URL:-$DEFAULT_VIRTUALMIN_URL/gpl/scripts}
DEFAULT_GIT_URL=${DEFAULT_GIT_URL:-https://raw.githubusercontent.com}
DEFAULT_VIRTUALMIN_GIT=${DEFAULT_VIRTUALMIN_GIT:-$DEFAULT_GIT_URL/virtualmin/virtualmin-install/master}
DEFAULT_VIRTUALMIN_GIT_MODULES_URL=${DEFAULT_VIRTUALMIN_GIT_MODULES_URL:-$DEFAULT_VIRTUALMIN_GIT/modules}

# Pre-download specific logic
args=("$@")
arg_count=$#
while [ $arg_count -gt 0 ]; do
  case "${args[0]}" in
    # Check for force flag passed to this installer
    --force | -f | --yes | -y)
      this__skipyesno="--force"
      ;;
    # Test connectivity to the provided URLs
    --test-conn | -T)
      urls=(
        "$DEFAULT_GIT_URL"
        "$DEFAULT_GITLAB_WIKISUITE_URL"
        "$DEFAULT_VIRTUALMIN_SCRIPTS_URL"
        "$DEFAULT_WIKISUITE_APT_URL"
        "$DEFAULT_TIKI_COMPOSER_URL"
      )
      ip_version="${args[1]:-}"  # Default to testing all
      failed=0
      # Determine which IP versions to test
      if [ "$ip_version" = "ipv4" ] || [ "$ip_version" = "ipv6" ]; then
        ip_versions=("$ip_version")
        shift
        ((arg_count--))
      else
        ip_versions=("ipv4" "ipv6")
      fi
      # Test connections for each URL and IP version
      for url in "${urls[@]}"; do
        for ip in "${ip_versions[@]}"; do
          test_connection "$url" "$ip" || ((failed++))
        done
      done
      # Exit with an error if any tests failed
      [ $failed -gt 0 ] && exit 1
      exit 0
      ;;
    # Run APT dry-run test
    --dry-run-apt | -D)
      urls=(
        "$DEFAULT_WIKISUITE_APT_URL/\$ID \$VERSION_CODENAME main arch=amd64"
        "$DEFAULT_VIRTUALMIN_URL/vm/7/gpl/apt virtualmin main"
        "https://apt.syncthing.net/ syncthing stable"
        "https://cloud.r-project.org/bin/linux/debian bookworm-cran40/"
        "https://packages.sury.org/php/ bookworm main"
        "https://deb.nodesource.com/node_20.x nodistro main"
        "https://repo.manticoresearch.com/repository/manticoresearch_bookworm bookworm main"
        "https://artifacts.elastic.co/packages/oss-6.x/apt stable main"
      )
      failed=0
      # Print info message about log file
      echo -e "${TURQUOISE}[INFO]${RESET} APT dry-run test started. Log is" \
        "written to ${wikisuite_dry_run}" | \
          tee >(log_ready | sed 's/\..*//' >> "$wikisuite_dry_run")
      # Clear and create the log file
      : > "$wikisuite_dry_run"
      # Run the APT dry-run test for each URL
      for url in "${urls[@]}"; do
        repo=$(echo "$url" | awk '{print $1 " " $2 " " $3}')
        params=$(echo "$url" | awk '{$1=""; $2=""; $3=""; print $0}' | xargs)
        dry_run_apt "$repo" "$params"  || ((failed++))
        printf "\n" >> "$wikisuite_dry_run"
      done
      # Exit with an error if any tests failed
      [ $failed -gt 0 ] && exit 1
      exit 0
      ;;
  esac
  # Shift copied array
  args=("${args[@]:1}")
  ((arg_count--))
done

# Function to download files using available tools
download_remote() {
  local url="$1"
  local output_file="$2"
  local timeout=5
  local retries=1
  local domain
  domain=$(echo "$url" | awk -F[/:] '{print $4}')
  local network_error_msg="Error: Unable to connect to \"$domain\" — check your network connection"

  # Try wget first
  if command -v wget &>/dev/null; then
    wget -q --timeout="$timeout" --tries="$retries" "$url" -O "$output_file" || {
      echo "$network_error_msg" >&2
      return 1
    }

  # Try curl if wget is not available
  elif command -v curl &>/dev/null; then
    curl -sSL --retry "$retries" --retry-delay 1 --max-time "$timeout" "$url" -o "$output_file" || {
      echo "$network_error_msg" >&2
      return 1
    }

  # Try fetch as last resort (mainly for BSD systems)
  elif command -v fetch &>/dev/null; then
    fetch -T "$timeout" -o "$output_file" "$url" || {
      echo "$network_error_msg" >&2
      return 1
    }

  else
    echo "Error: No HTTP client found. Install \"curl\", \"wget\", or \"fetch\"." >&2
    return 1
  fi
}

# URLs of files to download
download_urls=(
  "${DEFAULT_VIRTUALMIN_SCRIPTS_URL}/virtualmin-install.sh"
  "${DEFAULT_VIRTUALMIN_GIT_MODULES_URL}/virtualmin-install-modules.sh"
  "${DEFAULT_VIRTUALMIN_GIT_MODULES_URL}/virtualmin-install-module-s3-scheduled-backups.sh"
)

# Track downloaded files for cleanup
downloaded_files=()

# Download files if they do not exist
for url in "${download_urls[@]}"; do
  filename=$(basename "$url")
  download_remote "$url" "$filename" || {
    exit 1
  }
  downloaded_files+=("$(pwd)/$filename")
done

# Function to cleanup downloaded files
cleanup_downloads() {
  for file in "${downloaded_files[@]}"; do
    rm -f "$file"
  done
}

# Run the Virtualmin installation script with the provided shared arguments

# shellcheck disable=SC2086
set -- $this__skipyesno --hostname "$HOST" --module virtualmin-install-modules "$@"
# shellcheck disable=SC1091
. ./virtualmin-install.sh
