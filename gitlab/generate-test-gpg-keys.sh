#!/bin/bash

#
# Usage: generate-test-gpg-keys.sh <PATH TO EXPORT FOLDER> <BRANCH TO SKIP>...
#
# First argument is the export folder where the keys will be saved
# All other arguments are branchs that we want to skip generating test keys

EXPORT_FOLDER=$1
shift; # remove the folder from the arguments, the rest will be branches

PUB_KEY="${EXPORT_FOLDER}/gpgTestKeyFilePub"
PRIV_KEY="${EXPORT_FOLDER}/gpgTestKeyFilePriv"

# Do not generate test keys if we have a variable with a key
if [ -n "${GPG_PRIVATE_KEY}" ]; then
  echo "There is a env variable GPG_PRIVATE_KEY already defined, exiting without doing any work"
  exit 0
fi

# Do not generate test keys for branch that should really have keys, that will likely be a config issue
for SKIP_BRANCH in $* ; do
  if [ "${CI_COMMIT_BRANCH}" == "${SKIP_BRANCH}" ] ; then
    echo "Current CI branch (${CI_COMMIT_BRANCH}) is to be skipped, exiting without doing any work"
    echo "If this is the pipeline in the upstream repo, then make sure GPG_PRIVATE_KEY variable is defined in Gitlab CI/CD"
    exit 0
  fi
done

if [ ! -d "${EXPORT_FOLDER}" ] ; then
  echo "Target folder (${EXPORT_FOLDER}) does not exist, attempting to create it..."
  mkdir "${EXPORT_FOLDER}"
fi

if [ ! -d "${EXPORT_FOLDER}" ] ; then
  echo "Target folder (${EXPORT_FOLDER}) does not exist, refusing to generate keys, exiting with error without doing any work"
  exit 1
fi

if [ -f "${PRIV_KEY}" ] ; then
  echo "Private Key already exists (${PRIV_KEY}) , exiting without doing any work"
  exit 0
fi

echo "Generating gpg keys ..."

export GNUPGHOME="$(mktemp -d)"

# Generate Configuration
cat > $GNUPGHOME/keyConfig <<EOF
%echo Generating a default key for tests
Key-Type: RSA
Key-Length: 2048
Subkey-Type: default
Name-Real: WikiSuite Tester
Name-Comment: not to use in production
Name-Email: no-email-for-testing@wikisuite.org
Expire-Date: 0
%no-protection
# Do a commit here, so that we can later print "done" :-)
%commit
%echo done
EOF

# Generate keys
gpg --batch --generate-key $GNUPGHOME/keyConfig

# Export public and private key to a file
gpg --output "${EXPORT_FOLDER}/gpgTestKeyFilePub" --armor --export "WikiSuite Tester"
gpg --output "${EXPORT_FOLDER}/gpgTestKeyFilePriv" --armor --export-secret-key "WikiSuite Tester"

# Cleanup temp folder
rm -fr "$GNUPGHOME" 2> /dev/null

echo "Key files generated. Private key in ${EXPORT_FOLDER}/gpgTestKeyFilePriv"
